package company.com;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by heinr on 02.11.2016.
 */
public class SearchInFile {

    public void scannerSearch(String inputFile, String wordToSearch) throws Exception {

        if (inputFile.trim().equals("")) inputFile = "Istochnick.txt";
        if (wordToSearch.trim().equals("")) wordToSearch = "and";

        Scanner scanner = new Scanner(new File(inputFile));

        scanner.useDelimiter(System.getProperty("line.separator"));

        List<String> findingRows = new ArrayList<>();

        // цикл, пока не возмём все данные из файла
        while (scanner.hasNext()) {

            String lineOfFile = scanner.next();

            Scanner searchScaner = new Scanner(lineOfFile);

            String result = searchScaner.findInLine(wordToSearch);

            if (result != null) findingRows.add(lineOfFile);

        }

        scanner.close();

        for(String s: findingRows) System.out.println(s);
    }
}

