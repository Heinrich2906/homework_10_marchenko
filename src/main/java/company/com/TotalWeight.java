package company.com;

import java.io.File;
import java.util.*;

/**
 * Created by heinr on 02.11.2016.
 */
public class TotalWeight {

    public void maxWeight(String inputFile) throws Exception {

        Map<String, Integer> paar = new HashMap<String, Integer>();
        String mummalName;
        int count = 0;
        int max;

        if (inputFile.trim().equals("")) inputFile = "Tieregewicht.txt";

        Scanner scanner = new Scanner(new File(inputFile));

        scanner.useDelimiter(System.getProperty("line.separator"));

        List<String> findingRows = new ArrayList<>();

        // цикл, пока не возмём все данные из файла
        while (scanner.hasNext()) {

            String lineOfFile = scanner.next();

            String[] zwei = new String[2];

            zwei = lineOfFile.split(" ");

            if (paar.get(zwei[0]) == null) paar.put(zwei[0], new Integer(zwei[1]));
            else {
                count = (int) paar.get(zwei[0]);

                Integer proba = new Integer(zwei[1]);
                count = count + (int) proba;

                paar.put(zwei[0], count);
            }
        }

        scanner.close();

        mummalName = new String();
        max = -1;

        for (Map.Entry<String, Integer> entry : paar.entrySet()) {

            count = (int) entry.getValue();

            if (count > max) {
                max = count;
                mummalName = entry.getKey();
            }
        }

        System.out.println("" + mummalName + " " + max + " kg");
    }
}
