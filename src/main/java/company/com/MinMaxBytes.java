package company.com;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by heinr on 01.11.2016.
 */
public class MinMaxBytes {

    public void minMaxSearch(String inputFile) throws Exception {

        Integer minByte = new Integer(0);
        Integer maxByte = new Integer(0);

        int count;
        int min;
        int max;

        if (inputFile.trim().equals("")) inputFile = "Istochnick.txt";
        InputStream is = new FileInputStream(inputFile);

        Map<Integer, Integer> map = new HashMap<>();
        int symbol;
        char znak;

        do {

            symbol = is.read();
            znak = (char) symbol;

            if (symbol > 0) {

                System.out.println("Byte " + symbol + ", znak " + znak);

                if (map.get(symbol) == null) count = 0;
                else count = (int) map.get(symbol);

                count = count + 1;

                map.put(symbol, count);

            }
        } while (symbol > 0);

        is.close();

        min = map.size() + 1;
        max = -1;

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {

            count = (int) entry.getValue();

            if (count > max)
            {
                max = count;
                maxByte = entry.getKey();
            }
            if (count < min)
            {
                min = count;
                minByte = entry.getKey();
            }
        }

        System.out.println("Minumum " + minByte + ", Maximum " + maxByte);
    }

}
