package company.com;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;

/**
 * Created by heinr on 02.11.2016.
 */
public class Person implements Serializable
{
    private static final long serialVersionUID = 1L;

    String firstName;
    String lastName;
    final String finalString;
    transient PrintStream outputStream;
    transient String fullName;
    Main.Sex sex;


    Person(String firstName, String lastName, Main.Sex sex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = String.format("%s, %s", lastName, firstName);
        this.finalString = "final string";
        this.outputStream = System.out;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return firstName + " " +
                lastName + ", " +
                fullName + " " +
                sex + " " +
                finalString + "\n";
    }

    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {

        in.defaultReadObject();

        //Заполнение недостающих полей
        this.fullName = String.format("%s, %s", this.lastName, this.firstName);
        this.outputStream = System.out;
    }

}
