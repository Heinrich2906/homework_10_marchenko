package company.com;

import java.io.*;
import java.util.Scanner;

/**
 * Created by heinr on 02.11.2016.
 */
public class ReverseLines {
    public void mirrior(String inputFile, String outputFile) throws Exception {

        if (inputFile.trim().equals("")) inputFile = "Istochnick_5.txt";
        InputStream is = new FileInputStream(inputFile);

        if (outputFile.trim().equals("")) outputFile = "Poluchatel_6.txt";
        OutputStream os = new FileOutputStream(outputFile);

        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(outputFile));

        Scanner scanner = new Scanner(new File(inputFile));

        String lineSeporator = System.getProperty("line.separator");

        scanner.useDelimiter(lineSeporator);

        // цикл, пока не возмём все данные из файла
        while (scanner.hasNext()) {

            String lineOfFile = scanner.next();

            StringBuffer sbf = new StringBuffer(lineOfFile);

            lineOfFile = sbf.reverse().toString();

            fileWriter.write(lineOfFile + lineSeporator);

        }

        scanner.close();
        fileWriter.close();
    }
}

