package company.com;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by heinr on 02.11.2016.
 */
public class TextTransformation {
    public void evenNumberOfLetters(String inputFile, String outputFile) throws Exception {

        List<String> resultat = new ArrayList<>();

        if (inputFile.trim().equals("")) inputFile = "Istochnick_5.txt";
        InputStream is = new FileInputStream(inputFile);

        if (outputFile.trim().equals("")) outputFile = "Poluchatel_5.txt";
        OutputStream os = new FileOutputStream(outputFile);

        Scanner scanner = new Scanner(new File(inputFile));

        scanner.useDelimiter(System.getProperty("line.separator"));

        List<String> findingRows = new ArrayList<>();

        String prefix = new String();
        String result = new String();
        String myWord = new String();
        boolean firstRow = true;

        // цикл, пока не возмём все данные из файла
        while (scanner.hasNext()) {

            String lineOfFile = scanner.next();

            String[] allWords = new String[lineOfFile.length()];

            allWords = lineOfFile.split(" ");

            for (String s : allWords) {
                if (s.trim().equals("")) break;
                else {
                    if (s.trim().length() % 2 == 0) {
                        if (firstRow) {
                            prefix = "";
                            firstRow = false;
                        } else prefix = ", ";

                        myWord = s.substring(0, 1).toUpperCase() + s.substring(1);
                        result = result + prefix + myWord;
                    }
                }
            }

        }

        scanner.close();

        //Запись в файл
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(outputFile);
            fileWriter.write(result);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

