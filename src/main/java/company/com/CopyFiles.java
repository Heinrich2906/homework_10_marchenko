package company.com;

import java.io.*;

/**
 * Created by heinr on 01.11.2016.
 */

public class CopyFiles {

   public void basicStreams(String inputFile, String outputFile) throws Exception {

        if (inputFile.trim().equals("")) inputFile = "Istochnick.txt";
        InputStream is = new FileInputStream(inputFile);

        if (outputFile.trim().equals("")) outputFile = "Poluchatel.txt";
        OutputStream os = new FileOutputStream(outputFile);

        int symbol;
        char znak;

        do {
            symbol = is.read();
            znak = (char) symbol;
            if (symbol > 0) {
                System.out.println("Byte " + symbol + ", znak " + znak);
                os.write(symbol);
            }
        } while (symbol > 0);

        is.close();
        os.close();
    }
}
