package company.com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by heinr on 02.11.2016.
 */
public class Main {

    enum Sex {MALE, FAMALE}

    ;

    public static void main(String[] args) throws Exception {

        Main main = new Main();

//        main.Aufgabe_1();
//        main.Aufgabe_2();
//        main.Aufgabe_3();
//        main.Aufgabe_4();
//        main.Aufgabe_5();
//        main.Aufgabe_6();
        main.Aufgabe_7();
    }

    public void Aufgabe_1() throws Exception {

        String outputFile;  //исходящие данные
        String inputFile;   //входящие данные

        String newLine;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter a sourse file: ");
        inputFile = reader.readLine();

        System.out.print("Enter a traget file: ");
        outputFile = reader.readLine();

        CopyFiles copyFiles = new CopyFiles();
        copyFiles.basicStreams(inputFile, outputFile);

    }

    public void Aufgabe_2() throws Exception {

        String inputFile;   //входящие данные

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter a sourse file: ");
        inputFile = reader.readLine();

        MinMaxBytes minMaxBytes = new MinMaxBytes();
        minMaxBytes.minMaxSearch(inputFile);

    }

    public void Aufgabe_3() throws Exception {

        String inputFile;   //входящие данные
        String wordToSearch;//слово, которое надо искать

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter a sourse file: ");
        inputFile = reader.readLine();

        System.out.print("Enter a word to find: ");
        wordToSearch = reader.readLine();

        SearchInFile searchInFile = new SearchInFile();
        searchInFile.scannerSearch(inputFile, wordToSearch);

    }

    public void Aufgabe_4() throws Exception {

        String inputFile;   //входящие данные

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter a sourse file: ");
        inputFile = reader.readLine();

        TotalWeight totalWeight = new TotalWeight();
        totalWeight.maxWeight(inputFile);

    }

    public void Aufgabe_5() throws Exception {

        String outputFile;  //исходящие данные
        String inputFile;   //входящие данные

        String newLine;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter a sourse file: ");
        inputFile = reader.readLine();

        System.out.print("Enter a traget file: ");
        outputFile = reader.readLine();

        TextTransformation textTransformation = new TextTransformation();
        textTransformation.evenNumberOfLetters(inputFile, outputFile);

    }

    public void Aufgabe_6() throws Exception {

        String outputFile;  //исходящие данные
        String inputFile;   //входящие данные

        String newLine;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter a sourse file: ");
        inputFile = reader.readLine();

        System.out.print("Enter a traget file: ");
        outputFile = reader.readLine();

        ReverseLines reverseLines = new ReverseLines();
        reverseLines.mirrior(inputFile, outputFile);

    }

    public void Aufgabe_7() throws Exception {

        Person person = new Person("Heinrich", "Ilich", Sex.MALE);
        System.out.println(person.toString());

    }
}
